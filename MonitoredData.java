import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {
    String startTime;
    String endTime;
    String activity;

    MonitoredData() {
    }

    MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    List<MonitoredData> readData(String fileName) {
        List<MonitoredData> data = new ArrayList<>();
        try {
            data = Files.lines(new File(fileName).toPath())
                    .map(w -> w.split("\t\t"))
                    .map(s -> new MonitoredData(s[0], s[1], s[2]))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (MonitoredData m : data)
            System.out.println(m.activity);
        return data;
    }

    public void countDays(List<MonitoredData> data) {
        System.out.println("\nDistinct days: " + data.stream().map(s -> s.startTime.split(" ")[0]).distinct().count());
    }

    public void countActivities(List<MonitoredData> data) {
        Map<String, Long> myMap = data.stream().collect(Collectors.groupingBy(s -> s.activity, Collectors.counting()));

        for (String s : myMap.keySet()) {
            System.out.println(s + " " + myMap.get(s));
        }
    }

    public void countActivitiesEachDay(List<MonitoredData> data) {
        Map<String, Map<String, Long>> myMap = data.stream()
                .collect(Collectors.groupingBy(s -> s.startTime.split(" ")[0],
                        Collectors.groupingBy(s -> s.activity, Collectors.counting())));

        for (String s : myMap.keySet()) {
            System.out.println(s);
            for (String z : myMap.get(s).keySet()) {
                System.out.println("    " + z + " " + myMap.get(s).get(z));
            }
        }
    }

    public void activityDuration(List<MonitoredData> data) {
        data.stream().forEach(d -> System.out.println(d.activity + " " +
                ChronoUnit.MINUTES.between(
                        LocalDateTime.parse(d.startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                        LocalDateTime.parse(d.endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                )
                + " : " +
                ChronoUnit.SECONDS.between(
                        LocalDateTime.parse(d.startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                        LocalDateTime.parse(d.endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                ) % 60
        ));
    }

    public void activityOverallDuration(List<MonitoredData> data) {
        Map<String, Long> myMap = data.stream().collect(Collectors.groupingBy(d -> d.activity,
                Collectors.summingLong(d ->
                        ChronoUnit.SECONDS.between(
                                LocalDateTime.parse(d.startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                                LocalDateTime.parse(d.endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        )
                )
        ));

        for (String s : myMap.keySet()) {
            System.out.println(s + " " + myMap.get(s) + "s");
        }
    }

    public void activity90Percent(List<MonitoredData> data) {
        Map<String, List<Long>> myMap = data.stream().filter(d -> d.activity != null)
                .collect(Collectors.groupingBy(d -> activity,
                        Collectors.mapping(
                                d -> ChronoUnit.MINUTES.between(
                                        LocalDateTime.parse(d.startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                                        LocalDateTime.parse(d.endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                                ),
                                Collectors.toList()
                        )
                ));

        myMap.entrySet().stream().filter(s ->
                myMap.get(s).stream().count() * 0.9 <= myMap.get(s).stream().filter(d -> d < 5).count()
        ).forEach(s -> System.out.println(s));
    }

    public void activity90Percent2(List<MonitoredData> data) {
        Map<String, Integer> lessThan5Minutes = data.stream().collect(Collectors.groupingBy(d -> d.activity,
                Collectors.summingInt(
                        d -> ChronoUnit.MINUTES.between(
                                LocalDateTime.parse(d.startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                                LocalDateTime.parse(d.endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        ) < 5 ? 1 : 0)
        ));

        Map<String, Long> allActivities = data.stream().collect(Collectors.groupingBy(d -> d.activity, Collectors.counting()));

        lessThan5Minutes.entrySet().stream().filter(d -> d.getValue() >= 0.9 * allActivities.get(d.getKey()))
                .forEach(d -> System.out.println(d.getKey()));
    }

    public static void main(String args[]) {
        MonitoredData md = new MonitoredData();
        List<MonitoredData> data = md.readData("src/Activities.txt");
        md.countDays(data);
        md.countActivities(data);
        md.countActivitiesEachDay(data);
        md.activityDuration(data);
        md.activityOverallDuration(data);
        md.activity90Percent2(data);
    }
}